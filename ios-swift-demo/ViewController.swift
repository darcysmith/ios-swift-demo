//
//  ViewController.swift
//  ios-swift-demo
//
//  Created by D'Arcy Smith on 2017-07-07.
//  Copyright © 2017 TerraTap Technologies Inc. All rights reserved.
//

import TraceLog
import UIKit

class ViewController: UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        logTrace { "enter viewDidLoad" }
        logTrace { "exit viewDidLoad" }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        logTrace { "enter didReceiveMemoryWarning" }
        logTrace { "exit didReceiveMemoryWarning" }
    }
}
